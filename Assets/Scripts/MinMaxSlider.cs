﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class MinMaxSlider : MonoBehaviour {

    [System.Serializable]
    public class Vector2Event : UnityEvent<Vector2> {}

    public Slider m_MinSlider;
    public Slider m_MaxSlider;
    public float m_MinValue;
    public float m_MaxValue;
    public float m_MinSliderStartValue;
    public float m_MaxSliderStartValue;
    public bool m_WholeNumbers;
    public Vector2Event m_MinMaxChanged;

    private Vector2 m_MinMax;

    public void Start()
    {
        SetupSlider(m_MinSlider, m_MinSliderStartValue, OnMinChanged);
        SetupSlider(m_MaxSlider, m_MaxValue - m_MaxSliderStartValue, OnMaxChanged);

        m_MinMax = new Vector2(m_MinSliderStartValue, m_MaxSliderStartValue);
        m_MinMaxChanged.Invoke(m_MinMax);
    }

    private void SetupSlider(Slider slider, float startValue, UnityAction<float> eventFunction)
    {
        slider.minValue = m_MinValue;
        slider.maxValue = m_MaxValue;
        slider.wholeNumbers = m_WholeNumbers;
        slider.value = startValue;
        slider.onValueChanged.AddListener(eventFunction);
    }

    public void OnMinChanged(float value)
    {
        if (value > m_MinMax.y)
        {
            m_MinSlider.value = m_MinMax.y;
            m_MinMax.x = m_MinMax.y;
        }
        else
        {
            m_MinMax.x = value;
        }
        m_MinMaxChanged.Invoke(m_MinMax);
    }

    public void OnMaxChanged(float value)
    {
        if (m_MaxValue - value < m_MinMax.x)
        {
            m_MaxSlider.value = m_MaxValue - m_MinMax.x;
            m_MinMax.y = m_MinMax.x;
        }
        else
        {
            m_MinMax.y = m_MaxValue - value;
        }
        m_MinMaxChanged.Invoke(m_MinMax);
    }
}
