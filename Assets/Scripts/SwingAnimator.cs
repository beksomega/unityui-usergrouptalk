﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwingAnimator : MonoBehaviour
{
    public float m_Duration;
    public float m_MinZ;
    public float m_MaxZ;

    private RectTransform m_Rect;
    private float m_DurationReciprocal;
    private float m_AnimationStartTime;

    private void Start()
    {
        m_DurationReciprocal = 1 / m_Duration;
    }

    public void StartAnimation()
    {
        m_Rect = GetComponent<RectTransform>();
        m_AnimationStartTime = Time.time;
        StartCoroutine(Animate());
    }

    private IEnumerator Animate()
    {
        while ((Time.time - m_AnimationStartTime) < m_Duration)
        {
            Vector3 rotation = m_Rect.eulerAngles;
            float interpolation = getInterpolation((Time.time - m_AnimationStartTime) * m_DurationReciprocal);
            rotation.z = Mathf.Lerp(m_MinZ, m_MaxZ, interpolation);
            m_Rect.eulerAngles = rotation;
            yield return null;
        }
    }

    private static float bounce(float t)
    {
        return t * t * 8.0f;
    }
    public float getInterpolation(float t)
    {
        t *= 1.1226f;
        if (t < 0.3535f) return bounce(t);
        else if (t < 0.7408f) return bounce(t - 0.54719f) + 0.7f;
        else if (t < 0.9644f) return bounce(t - 0.8526f) + 0.9f;
        else return bounce(t - 1.0435f) + 0.95f;
    }

}
