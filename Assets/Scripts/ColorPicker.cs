﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Events;

public class ColorPicker : MonoBehaviour, IPointerDownHandler, IDragHandler
{
    [System.Serializable] //won't show up if it's not serializeable
    public class ColorEvent : UnityEvent<Color> { }

    public bool m_LogStuff;
    public ColorEvent m_ColorUpdated;

    private RectTransform m_Rect;
    private Texture2D m_Texture;

    private void Start()
    {
        m_Rect = GetComponent<RectTransform>();
        m_Texture = GetComponent<Image>().sprite.texture;
    }

    public void OnPointerDown(PointerEventData ped)
    {
        GetColor(ped);
    }

    public void OnDrag(PointerEventData ped)
    {
        GetColor(ped);
    }

    private void GetColor(PointerEventData ped)
    {
        //ped.position is in screen coordinates - need to fix that
        //if we wanted to know the position relative to the pivot we could just use:
        //RectTransformUtility.ScreenPointToLocalPointInRectangle
        //but the problem is we need to know the position relative to the *bottom left* (pixel coordinates)
        //not *necessarily* the pivot, so we need to do another convertion.

        //we could also convert from Screen -> World -> Relative, instead of Screen -> Local -> Relative | same same

        //Local
        Vector2 localPos;
        RectTransformUtility.ScreenPointToLocalPointInRectangle(m_Rect, ped.position, ped.pressEventCamera, out localPos);

        //Relative
        Vector3[] localCorners = new Vector3[4];
        m_Rect.GetLocalCorners(localCorners);
        Matrix4x4 matrix = Matrix4x4.TRS(localCorners[0], m_Rect.rotation, m_Rect.localScale);
        Vector2 relativePos = matrix.inverse.MultiplyPoint3x4(localPos);

        //Pixel
        Vector2 rectSize = new Vector2(Vector2.Distance(localCorners[0], localCorners[3]), Vector2.Distance(localCorners[0], localCorners[1]));
        Vector2 percentagePos = relativePos / rectSize;
        Vector2 texturePos = new Vector2(percentagePos.x * m_Texture.width, percentagePos.y * m_Texture.height);

        m_ColorUpdated.Invoke(m_Texture.GetPixel((int)texturePos.x, (int)texturePos.y));

        if (m_LogStuff)
        {
            Debug.Log("Screen Point: " + ped.position);
            Debug.Log("Relative to Pivot: " + localPos);
            Debug.Log("Relative to Bottom Left: " + relativePos);
            Debug.Log("Percentage Position, Relative to BL: " + percentagePos);
            Debug.Log("Texture Coordinates: " + texturePos);
        }
    }
}
