﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColorSetter : MonoBehaviour
{
    private Image m_Image;

    private void Start()
    {
        m_Image = GetComponent<Image>();
    }

    public void SetColor(Color color)
    {
        m_Image.color = color;
    }
}
