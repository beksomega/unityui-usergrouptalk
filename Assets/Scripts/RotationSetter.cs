﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationSetter: MonoBehaviour
{
    public List<RectTransform> m_Rects;
    public Vector3 m_Rotation;

    public void SetRotation()
    {
        foreach (RectTransform rect in m_Rects)
        {
            rect.eulerAngles = m_Rotation;
        }
    }
}
