﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DistanceScaler : MonoBehaviour {

    public Transform m_ParentRectTransform;
    public List<Transform> m_ChildrenRectTransforms;
	public AnimationCurve m_ResizingCurve;

    public void Start()
    {
        UpdateScale();
    }

    public void OnValueChanged(Vector2 normalizedPos)
    {
        UpdateScale();
    }

    private void UpdateScale()
    {
        foreach (RectTransform rect in m_ChildrenRectTransforms)
        {
            float scale = m_ResizingCurve.Evaluate(Vector3.Distance(m_ParentRectTransform.position, rect.position));
            rect.localScale = new Vector3(scale, scale, scale);
        }
    }
}
