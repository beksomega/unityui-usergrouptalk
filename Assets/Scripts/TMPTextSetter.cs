﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TMPTextSetter : MonoBehaviour {

    private TextMeshProUGUI m_Text;

    private void Awake()
    {
        m_Text = GetComponent<TextMeshProUGUI>();
    }

    public void SetTextToVector2(Vector2 v2)
    {
        m_Text.text = v2.ToString();
    }
}
