﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class HoverGrower : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public Vector2 m_Growth;

    private RectTransform m_RectTransform;

    private void Start()
    {
        m_RectTransform = GetComponent<RectTransform>();
    }

    public void OnPointerEnter(PointerEventData ped)
    {
        m_RectTransform.sizeDelta += m_Growth;
    }

    public void OnPointerExit(PointerEventData ped)
    {
        m_RectTransform.sizeDelta -= m_Growth;
    }
}
